
//: Playground - noun: a place where people can play

import UIKit

class Vehicle: NSObject {
    
    var speed = 0 {
        didSet {
            ride()
        }
    }
    
    func ride() {
        print("I'm riding with speed \(speed) km...")
    }
    
    func makeNoise() {
        print("E ron don don")
    }
}

let venicle = Vehicle()
venicle.makeNoise()

class Train: Vehicle {
    
    override init() {
        super.init()
        
        speed = 89
    }
    
    override func makeNoise() {
        print("ЧУХ ЧУХ")
    }
}

let train = Train()
train.makeNoise()
train.speed
train.ride()

let kek = [[1,1], [2,4]].flatMap { (<#Array<Int>#>) -> ElementOfResult? in
    <#code#>
}

