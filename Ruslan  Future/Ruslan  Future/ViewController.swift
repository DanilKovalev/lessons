//
//  ViewController.swift
//  Ruslan  Future
//
//  Created by Hadevs on 07.07.17.
//  Copyright © 2017 DevLabs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let gesture = UILongPressGestureRecognizer(target: self, action: #selector(self.imageViewTapped(sender:)))
        gesture.minimumPressDuration = 3
        imageView.addGestureRecognizer(gesture)
        
        imageView.isUserInteractionEnabled = true
    }
    
    func imageViewTapped(sender: UITapGestureRecognizer) {
        imageView.image = imageView.image == #imageLiteral(resourceName: "habstom") ? #imageLiteral(resourceName: "nikita"): #imageLiteral(resourceName: "habstom")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

