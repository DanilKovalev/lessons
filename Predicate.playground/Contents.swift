//: Playground - noun: a place where people can play

import UIKit

class Person: NSObject {
    let firstName: String
    let lastName: String
    let age: Int
    
    init(firstName: String, lastName: String, age: Int) {
        self.firstName = firstName
        self.lastName = lastName
        self.age = age
    }
    
    override var description: String {
        return "\(firstName) \(lastName)"
    }
}

let alice = Person(firstName: "Alice", lastName: "Smith", age: 24)
let bob = Person(firstName: "Bob", lastName: "Jones", age: 27)
let charlie = Person(firstName: "Charlie", lastName: "Smith", age: 33)
let quentin = Person(firstName: "Quentin", lastName: "Alberts", age: 31)

let people = [alice, bob, charlie, quentin]


// Map
var families: [String] = people.map { (chel) -> String in
    return chel.lastName
}

// Filter
var names: [Person] = people.filter { (chel) -> Bool in
    return chel.age == 33
}

// Sort
var persons: [Person] = people.sorted { (chel1, chel2) -> Bool in
    return chel1.age < chel2.age
}

let bobPredicate = NSPredicate(format: "firstName contains 'i'")
let smithPredicate = NSPredicate(format: "lastName = %@", "Smith")
let thirtiesPredicate = NSPredicate(format: "age >= 30")

(people as NSArray).filtered(using: bobPredicate)
// ["Bob Jones"]

(people as NSArray).filtered(using: smithPredicate)
// ["Alice Smith", "Charlie Smith"]

(people as NSArray).filtered(using: thirtiesPredicate)
// ["Charlie Smith", "Quentin Alberts"]











