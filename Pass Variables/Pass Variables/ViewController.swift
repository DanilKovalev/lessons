//
//  ViewController.swift
//  Pass Variables
//
//  Created by Hadevs on 30.06.17.
//  Copyright © 2017 DevLabs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "pizzaVC") as! PizzaViewController
        vc.pizza = "PEPEronnin"
        
        //present
//        present(vc, animated: true, completion: nil)
        //push
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

