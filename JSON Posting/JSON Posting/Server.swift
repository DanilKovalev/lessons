//
//  Server.swift
//  JSON Posting
//
//  Created by Hadevs on 05.07.17.
//  Copyright © 2017 DevLabs. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

struct Company {
    var name: String
    var city: String
}

class Server: NSObject {
    func loadCompanies(completion: @escaping (_ companies: [Company]) -> Void) {
        
        var companies: [Company] = []
        let _ = Alamofire.request("http://89.223.26.183:27018/companies", method: .get)
        .responseSwiftyJSON { (request, response, json, error) in
            guard json.exists() else {
                print("JSON IS NOT EXIST")
                return
            }
            
            for object in json {
                let companyName = object.1["companyName"].stringValue
                let city = object.1["city"].stringValue
                let company = Company(name: companyName, city: city)
                companies.append(company)
            }
            
            completion(companies)
        }
    }
}
