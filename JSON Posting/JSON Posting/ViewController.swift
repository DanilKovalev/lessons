//
//  ViewController.swift
//  JSON Posting
//
//  Created by Hadevs on 05.07.17.
//  Copyright © 2017 DevLabs. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        Server().loadCompanies { (companies) in
            let array = companies.map({ (company) -> String in
                return company.name + ", " + company.city
            })
            
            print(array)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
