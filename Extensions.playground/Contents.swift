
//: Playground - noun: a place where people can play

import UIKit

extension String {
    func removeSpacesAndAddPlus() -> String {
        return replacingOccurrences(of: " ", with: "") + " +"
    }
}

extension Int {
    func square() -> Int {
        return self * self
    }
}

var str = "He ll o, playg round"

str = str.removeSpacesAndAddPlus()

let h = 3.square().square()


extension NSObject {
    func generateRandomNumber() -> String {
        var returned = ""
        let ended = arc4random_uniform(12) + 3
        for _ in 0..<ended {
            let number = arc4random_uniform(10)
            returned += "\(number)"
        }
        
        return returned
    }
}

"ge".generateRandomNumber()