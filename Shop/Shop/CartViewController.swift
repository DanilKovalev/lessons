//
//  CartViewController.swift
//  Shop
//
//  Created by Hadevs on 30.06.17.
//  Copyright © 2017 DevLabs. All rights reserved.
//

import UIKit

class CartViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var hexLabel: UILabel!
    @IBOutlet weak var colorView: UIView!
    
    var product: Product!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        nameLabel.text = product.name
        hexLabel.text = product.hex
        colorView.backgroundColor = product.color
        
        title = "\(product.name) (\(product.hex))"
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        colorView.layer.cornerRadius = colorView.frame.height / 2
    }

 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
