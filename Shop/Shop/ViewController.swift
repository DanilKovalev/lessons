//
//  ViewController.swift
//  Shop
//
//  Created by Hadevs on 30.06.17.
//  Copyright © 2017 DevLabs. All rights reserved.
//

import UIKit

struct Product {
    var name: String
    var color: UIColor
    var hex: String
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var catalogTableView: UITableView!
    
    var products: [Product] = [
        Product(name: "Красный", color: .red, hex: "#ff0000"),
        Product(name: "Зеленый", color: .green, hex: "#00ff00"),
        Product(name: "Синий", color: .blue, hex: "#0000ff")
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //delegating
        catalogTableView.delegate = self
        catalogTableView.dataSource = self
        
        //title
        title = "Выберите товар"
        
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true) // Для красоты
        let index = indexPath.row
        
        let singleVC = storyboard?.instantiateViewController(withIdentifier: "nikitaSuka") as! CartViewController
        singleVC.product = products[index]
        
        navigationController?.pushViewController(singleVC, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "colorCell", for: indexPath) as! ColorTableViewCell
        cell.configure(color: products[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
