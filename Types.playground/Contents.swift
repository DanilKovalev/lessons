//: Playground - noun: a place where people can play

import UIKit


// where is, приведение типов
var str = [1, 3, 10, 512, 55, 66, 1, 0]

for number in str where number % 2 != 0 {
    let index = str.index(of: number)
    str[index!] += 1
}

// Конвертация
let ageLelik = "12"
let age = Int8(ageLelik)!
let k = true
let strK = String(k)
